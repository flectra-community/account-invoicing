# Copyright 2021 Camptocamp (https://www.camptocamp.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Account Move Propagate Ref",
    "summary": "Propagate ref when reversing and recreating an accounting move",
    "version": "2.0.1.0.1",
    "category": "Generic Modules/Account",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/account-invoicing",
    "depends": ["account"],
    "license": "AGPL-3",
    "installable": True,
}
