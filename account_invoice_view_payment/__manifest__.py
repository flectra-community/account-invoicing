# Copyright 2017 ForgeFlow S.L.
#        (https://www.eficent.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html)

{
    "name": "Account Invoice View Payment",
    "summary": "Access to the payment from an invoice",
    "version": "2.0.1.0.2",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/account-invoicing",
    "author": "ForgeFlow, " "Odoo Community Association (OCA)",
    "category": "Accounting",
    "depends": ["account"],
    "data": ["views/account_payment_view.xml", "views/account_move_view.xml"],
    "installable": True,
}
