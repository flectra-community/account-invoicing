# Copyright 2019 ForgeFlow S.L. (https://www.forgeflow.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Account invoice search by reference",
    "version": "2.0.1.0.0",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "category": "Invoicing",
    "website": "https://gitlab.com/flectra-community/account-invoicing",
    "license": "AGPL-3",
    "depends": ["account"],
    "installable": True,
    "auto_install": False,
}
